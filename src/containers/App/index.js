import { useState, useEffect } from 'react';
import Home from 'components/Home';
import Instruction from 'components/Instruction';
import MainGame from 'components/MainGame';
import mainBg from 'assets/space-background.png';
import './styles.scss';
import Parallax from 'parallax-js';

function SpaceApp() {
  const [content, setContent] = useState('home');

  const handleShowContent = (content) => {
    setContent(content);
  };
  useEffect(() => {
    const scene = document.getElementById('app');
    // eslint-disable-next-line no-unused-vars
    const paralax = new Parallax(scene);
  }, []);
  const selectSectionRender = (view) => {
    let template = '';
    switch (view) {
      case 'play':
        template = (
          <MainGame onClick={handleShowContent} />
        );
        break;
      case 'instruction':
        template = (
          <Instruction onClick={handleShowContent} />
        );
        break;
      default:
        template = (
          <Home onClick={handleShowContent} />
        );
        break;
    }
    return template;
  };

  return (
    <div className="container">
      <div id="app" className="mainbg">
        <img src={mainBg} alt="bg" data-depth="0.3" />
      </div>
      {
        selectSectionRender(content)
      }
    </div>
  );
}

export default SpaceApp;
