import PropTypes from 'prop-types';
import { memo } from 'react';
import { useDrop } from 'react-dnd';
// import className from 'classnames';
import './style.scss';
import Planet from 'components/Planet';

function DropZone({ data, onDrop, isFinish }) {
  const [{ isOver }, drop] = useDrop(() => ({
    accept: 'planet',
    drop: () => ({ ...data }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    })
  }));
  // const classNames = className('planet-name', {
  //   wrong: isFinish && (data.area !== data.planet.area)
  // });
  const handleDropFromZone = (planet, zone) => {
    onDrop(planet, zone, true);
  };
  return (
    <div className="drop-zone-container">
      {
        data.isFilled ? (
          <Planet data={data.planet} onDrop={handleDropFromZone} isPlaceholder isFinish={isFinish} isWrong={data.area !== data.planet.area} />
          // <div className="planet-droped">
          //   <img src={data.planet.image} alt={data.planet.name} />
          //   <div className={classNames}>
          //     <p>{data.planet.name}</p>
          //   </div>
          // </div>
        ) : (
          // eslint-disable-next-line jsx-a11y/aria-role
          <div className={`drop-zone ${isOver && 'active'}`} ref={drop} role="Dustbin" />
        )
      }
    </div>
  );
}

DropZone.propTypes = {
  data: PropTypes.object.isRequired,
  onDrop: PropTypes.func.isRequired,
  isFinish: PropTypes.bool.isRequired
};

export default memo(DropZone);
