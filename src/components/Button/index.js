import { memo } from 'react';
import PropTypes from 'prop-types';
import classname from 'classnames';
import './styles.scss';

function Button({
  className,
  style,
  variant,
  onClick,
  disabled,
  children,
  id
}) {
  const classNames = classname('space-app-button', className, variant);
  return (
    <>
      <button
        type="button"
        className={classNames}
        onClick={onClick}
        style={style}
        disabled={disabled}
        id={id}
      >
        {children}
      </button>
    </>
  );
}

Button.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  variant: PropTypes.string,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  children: PropTypes.node,
  id: PropTypes.string
};

Button.defaultProps = {
  className: '',
  style: {},
  variant: '',
  onClick: () => {},
  disabled: false,
  children: '',
  id: ''
};

export default memo(Button);
