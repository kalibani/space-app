/* eslint-disable jsx-a11y/aria-role */
import PropTypes from 'prop-types';
import { memo } from 'react';
import { useDrop } from 'react-dnd';
// import className from 'classnames';
import './style.scss';

function DropZoneDefault({ children }) {
  // eslint-disable-next-line no-unused-vars
  const [{ isOver }, drop] = useDrop(() => ({
    accept: 'planet',
    drop: () => ({ name: 'default' }),
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop()
    })
  }));
  return (
    <div className="drop-zone-default" ref={drop} role="Dustbin">
      {children}
    </div>
  );
}

DropZoneDefault.propTypes = {
  children: PropTypes.element.isRequired
};

export default memo(DropZoneDefault);
