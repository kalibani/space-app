import { useRef } from 'react';
import PropTypes from 'prop-types';
import { useDrag } from 'react-dnd';
import './style.scss';
import className from 'classnames';

function Planet({
  data, onDrop, isPlaceholder, isFinish,
  isWrong
}) {
  const ref = useRef(null);
  const [{ isDragging }, drag] = useDrag(() => ({
    type: 'planet',
    item: { ...data },
    end: (item, monitor) => {
      const dropResult = monitor.getDropResult();
      if (item && dropResult) {
        onDrop(item, dropResult);
      }
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
      handlerId: monitor.getHandlerId()
    })
  }));
  const classNames = className('planet', {
    'on-drag': isDragging,
    isPlaceholder
  });
  const classNameName = className('planetName', { wrong: isFinish && isWrong });
  return (
    // eslint-disable-next-line jsx-a11y/aria-role
    <div className={classNames} ref={isFinish ? ref : drag} role="Box" name="sdsd">
      <img alt="planet1" src={data.image} />
      <div className={classNameName}>
        <p>{data.name}</p>
      </div>
    </div>
  );
}

Planet.propTypes = {
  onDrop: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  isPlaceholder: PropTypes.bool.isRequired,
  isFinish: PropTypes.bool.isRequired,
  isWrong: PropTypes.bool.isRequired
};

export default Planet;
