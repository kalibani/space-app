import PropTypes from 'prop-types';
import Parallax from 'parallax-js';
import { memo, useEffect } from 'react';
import gameLogo from 'assets/game-logo.png';
import planet from 'assets/planet.png';
import ImageComponent from '../Image';
import Button from '../Button';
import './styles.scss';

function Home({ onClick }) {
  useEffect(() => {
    const scene = document.getElementById('scene-1');
    // eslint-disable-next-line no-unused-vars
    const parallaxInstance = new Parallax(scene);
    const scene2 = document.getElementById('scene-2');
    // eslint-disable-next-line no-unused-vars
    const parallaxInstance2 = new Parallax(scene2);
  }, []);
  return (
    <div className="home animate__animated animate__fadeIn" id="home">
      <div className="home-logo-wrapper" id="scene-2">
        <div data-depth="0.9">
          <ImageComponent urlImage={gameLogo} />
        </div>
      </div>
      <div className="home-content">
        <div className="home-image-wrapper" id="scene-1">
          <div data-depth="0.9">
            <ImageComponent urlImage={planet} width="90%" />
          </div>
        </div>
        <div className="home-button-wrapper">
          <Button onClick={() => onClick('play')}>
            Play the game
          </Button>
          <Button onClick={() => onClick('instruction')}>
            How to play
          </Button>
        </div>
      </div>
    </div>
  );
}

Home.propTypes = {
  onClick: PropTypes.func
};

Home.defaultProps = {
  onClick: () => {}
};

export default memo(Home);
