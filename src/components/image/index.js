import PropTypes from 'prop-types';
import { memo } from 'react';

function Image({ urlImage, width }) {
  return (
    <div className="space-app-image">
      <img src={urlImage} alt="game-logo" width={width} />
    </div>
  );
}

Image.defaultProps = {
  urlImage: '',
  width: ''
};

Image.propTypes = {
  urlImage: PropTypes.string,
  width: PropTypes.string
};

export default memo(Image);
