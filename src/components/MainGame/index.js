import PropTypes from 'prop-types';
import { useState, useEffect, useRef } from 'react';
import './style.scss';
import DropZone from 'components/DropZone';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import planet1 from 'assets/planet-01.png';
import planet2 from 'assets/planet-02.png';
import planet3 from 'assets/planet-03.png';
import planet4 from 'assets/planet-04.png';
import planet5 from 'assets/planet-05.png';
import space from 'assets/game-logo.png';

// components
import Planet from 'components/Planet';
import Image from 'components/Image';
import Button from 'components/Button';
import DropZoneDefault from 'components/DropZoneDefault';

const initZones = [
  {
    id: 'zone1',
    name: 'zone 1',
    isFilled: false,
    area: 1,
    planet: {}
  },
  {
    id: 'zone2',
    name: 'zone 2',
    isFilled: false,
    area: 2,
    planet: {}
  },
  {
    id: 'zone3',
    name: 'zone 3',
    isFilled: false,
    area: 3,
    planet: {}
  },
  {
    id: 'zone4',
    name: 'zone 4',
    isFilled: false,
    area: 4,
    planet: {}
  },
  {
    id: 'zone5',
    name: 'zone 5',
    isFilled: false,
    area: 5,
    planet: {}
  }
];

const initPlanets = [
  {
    image: planet1,
    name: 'Marvin X',
    id: 'planet1',
    area: 3
  },
  {
    image: planet2,
    name: 'Gibon Yupi',
    id: 'planet2',
    area: 2
  },
  {
    image: planet3,
    name: 'Pavlova',
    id: 'planet3',
    area: 5
  },
  {
    image: planet4,
    name: 'Gigola',
    id: 'planet4',
    area: 1
  },
  {
    image: planet5,
    name: 'Ectoearth',
    id: 'planet5',
    area: 4
  }
];

function MainGame({ onClick }) {
  const refPlanet = useRef(null);
  const refZone = useRef(null);
  const [isSubmit, setIsSubmit] = useState(false);
  const [isFinish, setIsFinish] = useState(false);
  const [isCorrect, setIsCorrect] = useState(false);
  const [zonesList, setZonesList] = useState([]);
  const [planetsList, setPlanetList] = useState([]);

  const initGame = () => {
    refPlanet.current = initPlanets;
    refZone.current = initZones;
    setPlanetList(initPlanets);
    setZonesList(initZones);
    setIsFinish(false);
    setIsCorrect(false);
    setIsSubmit(false);
  };
  useEffect(() => {
    initGame();
  }, []);

  const onDrop = (planet, zone, isPlaceholder = false) => {
    // update plate
    const filterPlanet = JSON.parse(JSON.stringify(refPlanet.current));
    const filtered = filterPlanet.filter((el) => el.id !== planet.id);
    if (zone.name === 'default') {
      filtered.push(planet);
    }
    setPlanetList([...filtered]);
    refPlanet.current = filtered;
    // update zone
    const updatedZone = [];
    refZone.current.forEach((el) => {
      if (el.id === zone.id) {
        updatedZone.push({
          ...el,
          isFilled: true,
          planet
        });
      } else if (isPlaceholder && el.planet.id === planet.id) {
        updatedZone.push({ ...el, planet: {}, isFilled: false });
      } else {
        updatedZone.push(el);
      }
    });
    refZone.current = updatedZone;
    setZonesList([...updatedZone]);
    if (filtered.length === 0) {
      setIsSubmit(true);
    } else {
      setIsSubmit(false);
    }
  };
  const handleSubmit = () => {
    let countCorrect = 0;
    zonesList.forEach((el) => {
      if (el.area === el.planet.area) {
        countCorrect += 1;
      }
    });
    setIsCorrect(countCorrect === 5);
    setIsSubmit(false);
    setIsFinish(true);
  };
  const backHome = () => {
    onClick('home');
  };
  return (
    <DndProvider backend={HTML5Backend}>
      <div className="main-game animate__animated animate__fadeIn">
        <Image urlImage={space} width="250px" />
        <div className="wrapper-zone">
          <p className="guide">Small</p>
          <div className="drop-zone-container">
            {
              zonesList.map((el) => <DropZone data={el} key={el.id} isFinish={isFinish} onDrop={onDrop} />)
            }
          </div>
          <p className="guide">Large</p>
        </div>
        <DropZoneDefault>
          <div className="planets">
            {
              planetsList.map((el) => <Planet key={el.id} data={el} onDrop={onDrop} isPlaceholder={false} isFinish={isFinish} isWrong={false} />)
            }
            {
              isFinish && (
                <div className="button-wrapper">
                  <Button className="animate__animated animate__fadeInUp" variant={!isCorrect ? 'primary-pink' : 'violet'} onClick={backHome}>{isCorrect ? 'CONGRATULATIONS!' : 'WOW ... YOU LOST!'}</Button>
                  <Button className="animate__animated animate__fadeInUp play-again-button" variant="outline" onClick={initGame}>PLAY AGAIN</Button>
                </div>
              )
            }
            {
              isSubmit && (<Button variant="outline" className="submit-button animate__animated animate__fadeInUp" onClick={handleSubmit}>SUBMIT</Button>)
            }

          </div>
        </DropZoneDefault>
      </div>
    </DndProvider>
  );
}

MainGame.propTypes = {
  onClick: PropTypes.func.isRequired
};

export default MainGame;
