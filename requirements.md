# Test Proposal

The goal of this task is to create a React-based web app game. During this task, you will need to show your skills in HTML/CSS, including JavaScript knowledge with ReactJs library.

![Game](./src/assets/game-screens/Game.png "Game")

## Overview

The idea of the game is to place planets in the correct order by size, from the smallest one to the largest. Each planet has its own name and image. There should be a placeholder for each planet. User by dragging the planet image with the help of a mouse drops the planet into the placeholder. When all planets are placed, a submit button should appear under the planet list. The results should be checked by clicking on this button. If the order is correct, then the user should see a congratulations message. If the order is incorrect, the user should see a fail message. Incorrect positions should be highlighted in red color.

## Gameplay

![Homepage](./src/assets/game-screens/Homepage.png "Homepage")
_Welcome screen_

When the user enters the web app of the game he should see a welcome screen with a title - “Space” and 2 buttons.

“**How to play**” button will show a short description of the game.

It contains the title “Space”, heading  “How to play?”, text block with scroll and “**Back to main**” button which should guide the user back to the welcome screen.

The text for the description can be taken from https://en.wikipedia.org/wiki/Space_exploration

![How to play](./src/assets/game-screens/How-to-Play.png "How to play")

“**Play the game**” button starts the game.
Buttons have white color text and a white outline with a transparent background. For the button hover effect set a background color as it’s shown in design. Pay attention that the “**Play the game**” button has a custom style.

The image of the background has a parallax effect on the mouse’s move. Similar to this  http://matthew.wagerfield.com/parallax/

All elements should be placed as shown in the image with the exact design.

![Game](./src/assets/game-screens/Game.png "Game")
_Game Screen_

The game screen contains the title “Space”, 5 round placeholders with white borders, two text labels “Small” and “Large” before and after the placeholder list and 5 planet images with names.

![Drag&Drop](./src/assets/game-screens/Game-Drag.png "Drag&Drop")

Users can drag the planets with the help of mouse (touch, click) any planet to any placeholder. Users should be able to remove the planet from the placeholder by moving the planet with the help of a mouse out of the placeholder zone.

One planet for one placeholder. That means if the placeholder is already used users can’t add more planets there. Users can move the planets between empty placeholders.

When the user has placed all 5 planets inside every placeholder, then a “**Submit**” button should appear. By clicking on “**Submit**” the button should disappear and the app should check if the position of planets are correct.

If the positions of planets are correct then the app should show a congratulations message, in case there are some mistakes, then it should show an error message and also highlights the wrong planet names with a red color.

![Game Lost](./src/assets/game-screens/Game-Lost.png "Game Lost")

![Game Won](./src/assets/game-screens/Game-Win.png "Game Won")

## Mobile version
This is not required, but up to you.

## Test package
We expect that after cloning your project we just need to run commands like composer install and/or yarn install/build to get this app running, if some extra steps are needed we expect them to be described in the readme file.

## Game Design
All preview images are added in the assets/game-screens folder. There is also a sketch file included in case you need something additional.

## Branch Workflow
By working on this task please use the branch workflow. Try to make a commit every hour and after you have done the task, create a pull request.

## In conclusion

### Javascript and HTML/CSS developer
* Before you start, reply when you are going to complete this test and how much time you're planning to spend on it.
* A single-page web application with Javascript and HTML/CSS
* All interfaces must be built with ReactJs Components
* You can use any open-source library for the development
* No need to secure the data
* Readme.md file about your web application, how set it up and how to use it
* In a few words, describe which part of the task was the most difficult one, why?
